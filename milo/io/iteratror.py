import numpy as np
from .batch import Batch
import torch
import math
from torch.autograd import Variable
from . import utils
from .utils import RandomShuffler


class Iterator(object):
    """Defines an iterator that loads batches of data from a Dataset.

    Attributes:
        dataset: The Dataset object to load Examples from.
        batch_size: Batch size.
        batch_size_fn: Function of three arguments (new example to add, current
            count of examples in the batch, and current effective batch size)
            that returns the new effective batch size resulting from adding
            that example to a batch. This is useful for dynamic batching, where
            this function would add to the current effective batch size the
            number of tokens in the new example.
        sort_key: A key to use for sorting examples in order to batch together
            examples with similar lengths and minimize padding. The sort_key
            provided to the Iterator constructor overrides the sort_key
            attribute of the Dataset, or defers to it if None.
        train: Whether the iterator represents a train set.
        repeat: Whether to repeat the iterator for multiple epochs.
        shuffle: Whether to shuffle examples between epochs.
        sort: Whether to sort examples according to self.sort_key.
            Note that repeat, shuffle, and sort default to train, train, and
            (not train).
        sort_within_batch: Whether to sort (in descending order according to
            self.sort_key) within each batch. If None, defaults to self.sort.
            If self.sort is True and this is False, the batch is left in the
            original (ascending) sorted order.
        device: Device to create batches on. Use -1 for CPU and None for the
            currently active GPU device.
    """

    def __init__(self, dataset, batch_size, vocabs, sort_key=None, device=None,
                 batch_size_fn=None, train=True,
                 repeat=None, shuffle=None, sort=None,
                 sort_within_batch=None, include_lengths=True):
        self.batch_size, self.train, self.dataset, self.vocabs = batch_size, train, dataset, vocabs
        self.batch_size_fn = batch_size_fn
        self.iterations = 0
        self.repeat = train if repeat is None else repeat
        self.shuffle = train if shuffle is None else shuffle
        self.sort = not train if sort is None else sort
        if sort_within_batch is None:
            self.sort_within_batch = self.sort
        else:
            self.sort_within_batch = sort_within_batch
        if sort_key is None:
            self.sort_key = dataset.sort_key
        else:
            self.sort_key = sort_key
        self.device = device
        if not torch.cuda.is_available() or self.device is None:
            self.device = -1
        self.include_lengths = include_lengths
        self.random_shuffler = RandomShuffler()
        
        # For state loading/saving only
        self._iterations_this_epoch = 0
        self._random_state_this_epoch = None
        self._restored_from_state = False

    def data(self):
        """Return the examples in the dataset in order, sorted, or shuffled."""
        if self.sort:
            xs = sorted(self.dataset, key=self.sort_key)
        elif self.shuffle:
            xs = [self.dataset[i] for i in self.random_shuffler(range(len(self.dataset)))]
        else:
            xs = self.dataset
        return xs

    def init_epoch(self):
        """Set up the batch generator for a new epoch."""

        if self._restored_from_state:
            self.random_shuffler.random_state = self._random_state_this_epoch
        else:
            self._random_state_this_epoch = self.random_shuffler.random_state

        self.create_batches()

        if self._restored_from_state:
            self._restored_from_state = False
        else:
            self._iterations_this_epoch = 0

        if not self.repeat:
            self.iterations = 0

    def create_batches(self):
        self.batches = []
        data = self.data()
        for i in range(0, len(data), self.batch_size):
            self.batches.append(data[i:min(i+self.batch_size, len(data))])
        
    def data_to_variable(self, minibatch):
        """
        
        :param minibatch: list of example (example.word list, example.word_char list of list, ...)
        :type minibatch: list
        :return: dict of padded variable
        :rtype: dict of Variable
        """
        minibatch = list(minibatch)
        var_batch = dict()
        for name, vocab in self.vocabs.items():
            data = [getattr(d, name) for d in minibatch]
            max_len = max(len(x) for x in data) + 1
            # init numpy
            if '_char' in name and name[:-5] in self.vocabs:
                max_char_len = min(max(len(y) for x in data for y in x) + utils.NUM_CHAR_PAD, utils.MAX_CHAR_LENGTH)
                id_inputs = np.empty([len(minibatch), max_len, max_char_len], dtype=np.int64)
                # fill numpy
                for i, words in enumerate(data):
                    example_size = len(words)
                    for j, chars in enumerate(words):
                        if len(chars) > max_char_len:
                            chars = chars[:max_char_len]
                        id_inputs[i, j, :len(chars)] = [vocab.stoi[c] for c in chars]
                        id_inputs[i, j, len(chars):] = vocab.stoi['<pad>']
                    id_inputs[i, example_size:, :] = vocab.stoi['<pad>']
                        
            else:
                id_inputs = np.empty([len(minibatch), max_len], dtype=np.int64)
                for i, words in enumerate(data):
                    example_size = len(words)
                    id_inputs[i, :example_size] = [vocab.stoi[w] for w in words]
                    id_inputs[i, example_size:] = vocab.stoi['<pad>']
            
            # numpy to variable on device
            arr = torch.from_numpy(id_inputs)
            if self.device == -1:
                arr = arr.contiguous()
            else:
                arr = arr.cuda(self.device)
            var_batch[name] = Variable(arr, volatile=not self.train)
        # process dense vector to Variable
        for name, finfo in self.dataset.fields.items():
            if finfo.get('maintrain_string', False):
                var_batch[name+'_string'] = [getattr(d, name+'_string') for d in minibatch]
            if finfo.get('dense', False):
                data = np.array([np.array(getattr(d, name)) for d in minibatch])
                max_len = max(len(x) for x in data) + 1
                dense_len = len(data[0][0])
                denseinputs = np.zeros([len(minibatch), max_len, dense_len], dtype=float)
                for i, sent in enumerate(data):
                    for j, token in enumerate(sent):
                        denseinputs[i][j] = data[i][j]
                arr = torch.from_numpy(denseinputs)
                if self.device == -1:
                    arr = arr.contiguous()
                else:
                    arr = arr.cuda(self.device)
                var_batch[name] = Variable(arr, volatile=not self.train, requires_grad=False)
        return var_batch

    @property
    def epoch(self):
        return math.floor(self.iterations / len(self))

    def __len__(self):
        if self.batch_size_fn is not None:
            raise NotImplementedError
        return math.ceil(len(self.dataset) / self.batch_size)

    def __iter__(self):
        while True:
            self.init_epoch()
            for idx, minibatch in enumerate(self.batches):
                # fast-forward if loaded from state
                if self._iterations_this_epoch > idx:
                    continue
                self.iterations += 1
                self._iterations_this_epoch += 1
                # TODO
                # if self.sort_within_batch:
                #     # NOTE: `rnn.pack_padded_sequence` requires that a minibatch
                #     # be sorted by decreasing order, which requires reversing
                #     # relative to typical sort keys
                #     if self.sort:
                #         minibatch.reverse()
                #     else:
                #         minibatch.sort(key=self.sort_key, reverse=True)
                minibatch = self.data_to_variable(minibatch)
                yield Batch(minibatch)
            if not self.repeat:
                return

    def state_dict(self):
        return {
            "iterations": self.iterations,
            "iterations_this_epoch": self._iterations_this_epoch,
            "random_state_this_epoch": self._random_state_this_epoch}

    def load_state_dict(self, state_dict):
        self.iterations = state_dict["iterations"]
        self._iterations_this_epoch = state_dict["iterations_this_epoch"]
        self._random_state_this_epoch = state_dict["random_state_this_epoch"]
        self._restored_from_state = True
        
        
class LenthBucketIterator(Iterator):
    """Defines an iterator that loads batches of data from a Dataset.

    Attributes:
        dataset: The Dataset object to load Examples from.
        batch_size: Batch size.
        batch_size_fn: Function of three arguments (new example to add, current
            count of examples in the batch, and current effective batch size)
            that returns the new effective batch size resulting from adding
            that example to a batch. This is useful for dynamic batching, where
            this function would add to the current effective batch size the
            number of tokens in the new example.
        sort_key: A key to use for sorting examples in order to batch together
            examples with similar lengths and minimize padding. The sort_key
            provided to the Iterator constructor overrides the sort_key
            attribute of the Dataset, or defers to it if None.
        train: Whether the iterator represents a train set.
        repeat: Whether to repeat the iterator for multiple epochs.
        shuffle: Whether to shuffle examples between epochs.
        sort: Whether to sort examples according to self.sort_key.
            Note that repeat, shuffle, and sort default to train, train, and
            (not train).
        sort_within_batch: Whether to sort (in descending order according to
            self.sort_key) within each batch. If None, defaults to self.sort.
            If self.sort is True and this is False, the batch is left in the
            original (ascending) sorted order.
        device: Device to create batches on. Use -1 for CPU and None for the
            currently active GPU device.
    """

    def __init__(self, dataset, batch_size, vocabs, sort_key=None, device=None,
                 batch_size_fn=None, train=True,
                 repeat=None, shuffle=None, sort=None,
                 sort_within_batch=None, bucket_lens=[5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 140]):
        super(LenthBucketIterator, self).__init__(dataset, batch_size, vocabs, sort_key=sort_key, device=device,
                                                 batch_size_fn=batch_size_fn, train=train,
                                                 repeat=repeat, shuffle=shuffle, sort=sort,
                                                 sort_within_batch=sort_within_batch)
        self.bucket_lens = bucket_lens
        if sort_key is None:
            self.tmp_sort_key = lambda x: len(getattr(x, list(self.vocabs.keys())[0]))
        else:
            self.tmp_sort_key = sort_key
        
        self.bucket_data = [[] for _ in self.bucket_lens]
        for x in self.dataset:
            for bucket_id, bucket_size in enumerate(self.bucket_lens):
                if self.tmp_sort_key(x) < bucket_size:
                    self.bucket_data[bucket_id].append(x)
                    break
        self.bucket_data = np.array([np.array(bdata) for bdata in self.bucket_data])
        self.bucket_variables = self.data_to_variable(self.bucket_data)
    
    def data_to_variable(self, bucket_data):
        bucket_sizes = [len(bucket_data[b]) for b in range(len(self.bucket_lens))]
        self.bucket_sizes = bucket_sizes
        data_variable = []

        for bucket_id in range(len(self.bucket_lens)):
            bucket_size = bucket_sizes[bucket_id]
            if bucket_size == 0:
                data_variable.append((1, 1))
                continue
            
            bucket_length = self.bucket_lens[bucket_id]
            var_bucket = dict()
            for name, vocab in self.vocabs.items():
                data = [getattr(d, name) for d in self.bucket_data[bucket_id]]
                # init numpy
                if '_char' in name and name[:-5] in self.vocabs:
                    max_char_len = min(max(len(y) for x in data for y in x) + utils.NUM_CHAR_PAD, utils.MAX_CHAR_LENGTH)
                    id_inputs = np.empty([bucket_size, bucket_length, max_char_len], dtype=np.int64)
                    # fill numpy
                    for i, words in enumerate(data):
                        example_size = len(words)
                        for j, chars in enumerate(words):
                            if len(chars)>max_char_len:
                                chars = chars[:max_char_len]
                            id_inputs[i, j, :len(chars)] = [vocab.stoi[c] for c in chars]
                            id_inputs[i, j, len(chars):] = vocab.stoi['<pad>']
                        id_inputs[i, example_size:, :] = vocab.stoi['<pad>']
    
                else:
                    id_inputs = np.empty([bucket_size, bucket_length], dtype=np.int64)
                    for i, words in enumerate(data):
                        example_size = len(words)
                        id_inputs[i, :example_size] = [vocab.stoi[w] for w in words]
                        id_inputs[i, example_size:] = vocab.stoi['<pad>']
    
                # numpy to variable on device
                arr = torch.from_numpy(id_inputs)
                if self.device == -1:
                    arr = arr.contiguous()
                else:
                    arr = arr.cuda(self.device)
                var_bucket[name] = Variable(arr, volatile=not self.train)
            # process dense vector to Variable
            for name, finfo in self.dataset.fields.items():
                if finfo.get('maintrain_string', False):
                    var_bucket[name + '_string'] = [getattr(d, name + '_string') for d in self.bucket_data[bucket_id]]
                if finfo.get('dense', False):
                    data = [getattr(d, name) for d in self.bucket_data[bucket_id]]
                    dense_len = len(data[0][0])
                    denseinputs = np.zeros([bucket_size, bucket_length, dense_len], dtype=float)
                    for i, sent in enumerate(data):
                        for j, token in enumerate(sent):
                            denseinputs[i][j] = data[i][j]
                    arr = torch.from_numpy(denseinputs)
                    if self.device == -1:
                        arr = arr.contiguous()
                    else:
                        arr = arr.cuda(self.device)
                    var_bucket[name] = Variable(arr, volatile=not self.train, requires_grad=False)
            data_variable.append(var_bucket)
        return data_variable
    
    def data(self):
        """Return the examples in the dataset in order, sorted, or shuffled."""
        if self.shuffle:
            xs = np.array([bucket_data[np.random.permutation(len(bucket_data))] for bucket_data in self.bucket_data])
        else:
            xs = self.bucket_data
        return xs

    def init_epoch(self):
        """Set up the batch generator for a new epoch."""

        if self._restored_from_state:
            self.random_shuffler.random_state = self._random_state_this_epoch
        else:
            self._random_state_this_epoch = self.random_shuffler.random_state

        self.create_batches()

        if self._restored_from_state:
            self._restored_from_state = False
        else:
            self._iterations_this_epoch = 0

        if not self.repeat:
            self.iterations = 0

    def create_batches(self):
        """Yield elements from data in chunks of batch_size."""
        total_size = float(sum(self.bucket_sizes))
        buckets_scale = [sum(self.bucket_sizes[:i + 1]) / total_size for i in range(len(self.bucket_sizes))]
        random_number = np.random.random_sample()
        bucket_id = min([i for i in range(len(buckets_scale)) if buckets_scale[i] > random_number])
        bucket_size = self.bucket_sizes[bucket_id]
        batch_size = min(bucket_size, self.batch_size)
        index = torch.randperm(bucket_size).long()[:batch_size]
        if self.device != -1:
            index = index.cuda(self.device)
        minibatch = dict()
        for name, _ in self.bucket_variables[bucket_id].items():
            if isinstance(self.bucket_variables[bucket_id][name], list) or \
                    isinstance(self.bucket_variables[bucket_id][name], np.ndarray):
                index_list = index.cpu().numpy()
                arr = [self.bucket_variables[bucket_id][name][i] for i in index_list]
            else:
                arr = self.bucket_variables[bucket_id][name][index]
            if name not in self.vocabs.keys():
                minibatch[name] = arr
                continue
            if hasattr(self.vocabs[name], 'singleton_freq') and hasattr(self.vocabs[name], 'singleton_replace') and self.vocabs[name].singleton_freq > 0 and self.vocabs[name].singleton_replace>0.0:
                ones = Variable(arr.data.new(arr.size()).fill_(1))
                single = (arr > self.vocabs[name].singletonid).long()
                noise = Variable(arr.data.new(arr.size()).float().bernoulli_(self.vocabs[name].singleton_replace).long())
                arr = arr * (ones - single * noise)
            minibatch[name] = arr
        yield minibatch

    def __iter__(self):
        while True:
            # fast-forward if loaded from state
            for idx, minibatch in enumerate(self.create_batches()):
                # if self._iterations_this_epoch > idx:
                #     continue
                self.iterations += 1
                self._iterations_this_epoch += 1
                # TODO
                # if self.sort_within_batch:
                #     # NOTE: `rnn.pack_padded_sequence` requires that a minibatch
                #     # be sorted by decreasing order, which requires reversing
                #     # relative to typical sort keys
                #     if self.sort:
                #         minibatch.reverse()
                #     else:
                #         minibatch.sort(key=self.sort_key, reverse=True)
                
                yield Batch(minibatch)
            if not self.repeat:
                return
    