import json
import codecs


class config(object):
    def __init__(self, file_path):
        with codecs.open(file_path, 'r', 'utf-8') as f:
            self.config = json.load(f)
    
    def get_readerconfig(self):
        return self.config['reader']
    
    def save(self, outpath):
        with codecs.open(outpath, 'w', 'utf-8') as f:
            json.dump(self.config, f)
