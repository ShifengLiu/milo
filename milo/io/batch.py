import torch


class Batch(object):
    """Defines a batch of examples along with its Fields.

    Attributes:
        batch_size: Number of examples in the batch.
        dataset: A reference to the dataset object the examples come from
            (which itself contains the dataset's Field objects).
        train: Whether the batch is from a training set.

    Also stores the Variable for each column in the batch as an attribute.
    """

    def __init__(self, data=None):
        """Create a Batch from a list of examples."""
        if data is not None:
            self.batch_size = len(list(data.values())[0])

            for (name, val) in data.items():
                setattr(self, name, val)

    def __len__(self):
        return self.batch_size
