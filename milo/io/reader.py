from . import config
from .vocab import NormVectors, SingletonVocab
from .iteratror import LenthBucketIterator, Iterator
from .dataset import SequenceTaggingDataset
from torch.autograd import Variable
import codecs
from collections import Counter, OrderedDict


def build_vocab(base_dataset, name, extend_dataset=[], unk_token='<unk>', pad_token='<pad>', singleton_freq=0, **kwargs):
    """Construct the Vocab object for this field from one or more datasets.

    Arguments:
        Positional arguments: Dataset objects or other iterable data
            sources from which to construct the Vocab object that
            represents the set of possible values for this field. If
            a Dataset object is provided, all columns corresponding
            to this field are used; individual columns can also be
            provided directly.
        Remaining keyword arguments: Passed to the constructor of Vocab.
    """
    if not isinstance(base_dataset, list):
        base_dataset = [base_dataset]
    if not isinstance(extend_dataset, list):
        extend_dataset = [extend_dataset]
    # count freqs from base datasets
    counter = Counter()
    sources = []
    for arg in base_dataset:
        if isinstance(arg, SequenceTaggingDataset):
            sources.append([getattr(example, name) for example in
                        arg])
        else:
            sources.append(getattr(arg, name))
    for data in sources:
        for x in data:
            counter.update(x)
    # count freqs from extended datasets
    extend_counter = Counter()
    extend_sources = []
    for arg in extend_dataset:
        if isinstance(arg, SequenceTaggingDataset):
            extend_sources.append([getattr(example, name) for example in
                        arg])
        else:
            extend_sources.append(getattr(arg, name))
    for data in extend_sources:
        for x in data:
            extend_counter.update(x)

    specials = list(OrderedDict.fromkeys(
        tok for tok in [unk_token, pad_token]
        if tok is not None))
    vocab = SingletonVocab(counter,
                           extend_counter,
                           singleton_freq=singleton_freq,
                           specials=specials,
                           **kwargs)
    return vocab


def build_charvocab(base_dataset, name, extend_dataset=[], **kwargs):
    """Construct the Vocab object for this field from one or more datasets.

    Arguments:
        Positional arguments: Dataset objects or other iterable data
            sources from which to construct the Vocab object that
            represents the set of possible values for this field. If
            a Dataset object is provided, all columns corresponding
            to this field are used; individual columns can also be
            provided directly.
        Remaining keyword arguments: Passed to the constructor of Vocab.
    """
    assert '_char' in name
    if not isinstance(base_dataset, list):
        base_dataset = [base_dataset]
    if not isinstance(extend_dataset, list):
        extend_dataset = [extend_dataset]
    # count freqs from base datasets
    counter = Counter()
    sources = []
    for arg in base_dataset:
        if isinstance(arg, SequenceTaggingDataset):
            sources.extend([getattr(example, name) for example in arg])
        else:
            sources.append(getattr(arg, name))
    for data in sources:
        for x in data:
            counter.update(x)
    # count freqs from extended datasets
    extend_counter = Counter()
    extend_sources = []
    for arg in extend_dataset:
        if isinstance(arg, SequenceTaggingDataset):
            extend_sources.extend([getattr(example, name) for example in arg])
        else:
            extend_sources.append(getattr(arg, name))
    for data in extend_sources:
        for x in data:
            extend_counter.update(x)
    vocab = SingletonVocab(counter,
                           extend_counter,
                           singleton_freq=0,
                           lower=False,
                           norm_digit=False,
                           ** kwargs)
    return vocab


class FileReader(object):
    def __init__(self, file_config):
        self.file_config = file_config
        self.config = config(file_config).get_readerconfig()
        
        self.data = self.init_data()
        self.build_vocab()
        self.iter = self.init_iter()
    
    def init_data(self):
        corpus = dict()
        fields = {}
        fields.update(self.config['infos'])
        fields.update(self.config['labels'])
        for name, path in self.config['path'].items():
            corpus[name] = SequenceTaggingDataset(path, fields)
        return corpus
    
    def build_vocab(self):
        self.vocabs = dict()
        
        for name, label_field in self.config['labels'].items():
            self.vocabs[name] = build_vocab(base_dataset=[self.data['train']], name=name, unk_token=None)
        for name, info_field in self.config['infos'].items():
            singleton_freq = info_field.get('singleton', 0)
            singleton_replace = info_field.get('singleton_replace', 0)
            lower = True #info_field.get('lower', False)
            norm_digit = info_field.get('normalize_digits', False)
            embed_path = info_field.get('embedding_path', None)
            use_char = info_field.get('usechar', False)
            dense = info_field.get('dense', False)
            if dense:
                continue
            vectors = NormVectors(path=embed_path, norm_digit=norm_digit, lower=lower) if embed_path is not None else None
            self.vocabs[name] = build_vocab(base_dataset=[self.data['train']],
                                            name=name,
                                            extend_dataset=[self.data['dev'], self.data['test']],
                                            singleton_freq=singleton_freq,
                                            lower=lower,
                                            norm_digit=norm_digit,
                                            vectors=vectors,
                                            singleton_replace=singleton_replace)
            if use_char:
                self.vocabs[name+'_char'] = build_charvocab(base_dataset=[self.data['train']],
                                                            name=name+'_char')
                
    def init_iter(self):
        # TODO
        # to add flexible construction for datasets with different name
        # to add deploy iter
        iterator = dict()
        iterator['train'] = LenthBucketIterator(dataset=self.data['train'],
                                                batch_size=self.config['batch_size'],
                                                vocabs=self.vocabs,
                                                device=self.config['device'],
                                                sort_key=lambda x: len(getattr(x, list(self.vocabs.keys())[0])))
        iterator['dev'] = Iterator(dataset=self.data['dev'],
                                   batch_size=self.config['annotate_batch_size'],
                                   vocabs=self.vocabs,
                                   device=self.config['device'],
                                   train=False,
                                   sort=False)
        iterator['test'] = Iterator(dataset=self.data['test'],
                                    batch_size=self.config['annotate_batch_size'],
                                    vocabs=self.vocabs,
                                    device=self.config['device'],
                                    train=False,
                                    sort=False)
        return iterator
    
    def get_singletonID(self, name):
        return self.vocabs[name].singletonid
    
    def out_prediction(self, batch, pred, mask, seq, out=''):
        pred_name = seq[-1]
        if isinstance(pred, Variable):
            pred = pred.data.cpu().numpy()
        else:
            pred = pred.cpu().numpy()
        seq = seq[:-1]
        with codecs.open(out, 'a', 'utf-8') as fout:
            infos = [getattr(batch, name).data.cpu().numpy() for name in seq]
            batch_size = batch.batch_size
            lengths = mask.data.sum(dim=1).long()
            for i in range(batch_size):
                for j in range(lengths[i]):
                    out = []
                    for name, info in zip(seq, infos):
                        out.append(self.vocabs[name].itos[info[i][j]])
                    out.append(self.vocabs[pred_name].itos[pred[i][j]])
                    fout.write(' '.join(out)+'\n')
                fout.write('\n')
