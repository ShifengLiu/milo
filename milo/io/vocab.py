import torch
import numpy as np
from collections import defaultdict
from .utils import _default_unk_index, hasembedding, norm_digits


class SingletonVocab(object):
    def __init__(self, counter, extended_counter, max_size=None, min_freq=1, specials=['<pad>'],
                 vectors=None, lower=True, norm_digit=True, singleton_freq=1, singleton_replace=0.0):
        assert singleton_freq == 0 or vectors is not None
        self.singleton_freq = singleton_freq
        self.singleton_replace = singleton_replace
        self.lower = lower
        self.norm_digit = norm_digit
        self.freqs = counter
        counter = counter.copy()
        extended_counter = extended_counter.copy()
        min_freq = max(min_freq, 1)
    
        self.itos = list()
        # frequencies of special tokens are not counted when building vocabulary
        # in frequency order
        for tok in specials:
            del counter[tok]
            del extended_counter[tok]

        self.singletons = set([word for word, freq in counter.items() if freq <= self.singleton_freq])
        
        max_size = None if max_size is None else max_size + len(self.itos)
    
        # sort by frequency, then alphabetically
        words_and_frequencies = sorted(counter.items(), key=lambda tup: tup[0])
        words_and_frequencies.sort(key=lambda tup: tup[1], reverse=True)
        
        for word, freq in words_and_frequencies:
            if freq < min_freq or freq <= self.singleton_freq:
                break
            self.itos.append(word) # frequent word only
            
        if max_size is not None and len(self.itos) > max_size:
            self.itos = self.itos[:max_size]

        extended_words = [w for w in extended_counter if
                          w not in self.itos and w not in self.singletons and (self.singleton_freq == 0 or hasembedding(w, vectors, self.lower, self.norm_digit))]
        self.itos += extended_words
        self.singletonid = len(self.itos)-1+len(list(specials))
        embed_singletons = [w for w in self.singletons if (self.singleton_freq == 0 or hasembedding(w, vectors, self.lower, self.norm_digit))]
        self.itos += embed_singletons
        self.itos = list(specials) + self.itos
        self.stoi = defaultdict(_default_unk_index)
        # stoi is simply a reverse dict for itos
        self.stoi.update({tok: i for i, tok in enumerate(self.itos)})
        
        self.vectors = None
        if vectors is not None:
            self.load_vectors(vectors)
            
    def __eq__(self, other):
        if self.freqs != other.freqs:
            return False
        if self.stoi != other.stoi:
            return False
        if self.itos != other.itos:
            return False
        if self.vectors != other.vectors:
            return False
        return True

    def __len__(self):
        return len(self.itos)
    
    def load_vectors(self, vectors):
        """
        Arguments:
            vectors: NormVector
            Remaining keyword arguments: Passed to the constructor of Vectors classes.
        """
        if not isinstance(vectors, list):
            vectors = [vectors]
        tot_dim = sum(v.dim for v in vectors)
        self.vectors = np.empty([len(self), tot_dim])
        for i, token in enumerate(self.itos):
            start_dim = 0
            for v in vectors:
                end_dim = start_dim + v.dim
                self.vectors[i][start_dim:end_dim] = v[token.strip()]
                start_dim = end_dim
            assert(start_dim == tot_dim)
        self.vectors = torch.from_numpy(self.vectors)
            

class NormVectors(object):

    def __init__(self, path, dim=-1, lower=True, norm_digit=True):
        self.path = path
        self.dim = dim
        self.lower = lower
        self.norm_digit = norm_digit
        self.load(path)
        
    def __getitem__(self, token):
        if token in self.vectors.keys():
            return self.vectors[token]
        elif self.lower and token.lower() in self.vectors.keys():
            return self.vectors[token.lower()]
        elif self.norm_digit and norm_digits(token) in self.vectors.keys():
            return self.vectors[norm_digits(token)]
        # elif self.lower and self.norm_digit and norm_digits(token.lower()) in self.vectors.keys():
        #     return self.vectors[norm_digits(token.lower())]
        # elif '<unk>' in self.vectors.keys():
        #     return self.vectors['<unk>']
        # elif 'UNK' in self.vectors.keys():
        #     return self.vectors['UNK']
        else:
            return np.random.uniform(-np.sqrt(3.0 / self.dim), np.sqrt(3.0 / self.dim), [1, self.dim]).astype(np.float32)
    
    def load(self, path):
        self.vectors = dict()
        with open(path, 'r') as file:
            for line in file:
                line = line.strip()
                # line = line.decode('utf-8')
                if len(line) == 0:
                    continue
                tokens = line.split()
                if self.dim == -1:
                    self.dim = len(tokens) - 1
                assert (self.dim + 1 == len(tokens))
                embedd = np.empty([1, self.dim], dtype=np.float32)
                embedd[:] = tokens[1:]
                token = tokens[0]
                if self.lower:
                    token = token.lower()
                if self.norm_digit:
                    token = norm_digits(token)
                self.vectors[token] = embedd
                # if self.lower and tokens[0].lower() not in self.vectors.keys():
                #     # print('lower', tokens[0], tokens[0].lower())
                #     self.vectors[tokens[0].lower()] = embedd
                # elif self.norm_digit and norm_digits(tokens[0]) not in self.vectors.keys():
                #     # print('norm digit', tokens[0], norm_digits(tokens[0]))
                #     self.vectors[norm_digits(tokens[0])] = embedd
                # elif self.lower and self.norm_digit and norm_digits(tokens[0].lower()) not in self.vectors.keys():
                #     # print('lower norm digit', tokens[0], norm_digits(tokens[0].lower()))
                #     self.vectors[norm_digits(tokens[0].lower())] = embedd

