from .examples import Example
from .utils import norm_digits


class SequenceTaggingDataset(object):
    """Defines a dataset for sequence tagging. Examples in this dataset
    contain paired lists -- paired list of words and tags.

    For example, in the case of part-of-speech tagging, an example is of the
    form
    [I, love, PyTorch, .] paired with [PRON, VERB, PROPN, PUNCT]

    See torchtext/test/sequence_tagging.py on how to use this class.
    """
    
    @staticmethod
    def sort_key(example):
        for attr in dir(example):
            if not callable(getattr(example, attr)) and \
                    not attr.startswith("__"):
                return len(getattr(example, attr))
        return 0
    
    def __init__(self, path, fields):
        """

        :param path: file path
        :type path: str
        :param fields: fiels[name] = {column, lower, norm_digit, usechar, singleton]
        :type fields: dict
        """
        examples = []
        columns = {}
        for name, field in fields.items():
            assert field.get('column', None) is not None
        with open(path) as input_file:
            for line in input_file:
                line = line.strip()
                if line == "":
                    if columns:
                        examples.append(Example.fromdict(columns))
                    columns = {}
                else:
                    infos = line.split()
                    for name, finfo in fields.items():
                        if name not in columns.keys():
                            columns[name] = []
                        if finfo.get('maintrain_string', False):
                            if name + '_string' not in columns.keys():
                                columns[name + '_string'] = []
                        tinfo = infos[finfo['column']]
                        if finfo.get('maintrain_string', False):
                            columns[name + '_string'].append(tinfo)
                        if finfo.get('dense', False):
                            tinfo = [float(t) for t in tinfo.split(',')]
                        if finfo.get('lower', False):
                            tinfo = tinfo.lower()
                        if finfo.get('normalize_digits', False):
                            tinfo = norm_digits(tinfo)
                        columns[name].append(tinfo)
                        if finfo.get('usechar', False):
                            if name + '_char' not in columns.keys():
                                columns[name + '_char'] = []
                            columns[name + '_char'].append(list(infos[finfo['column']]))
            if columns:
                examples.append(Example.fromdict(columns))
        
        self.examples = examples
        self.fields = dict(fields)
        # Unpack field tuples
        for n, f in list(self.fields.items()):
            if isinstance(n, tuple):
                self.fields.update(zip(n, f))
                del self.fields[n]
    
    def __getitem__(self, i):
        return self.examples[i]
    
    def __len__(self):
        try:
            return len(self.examples)
        except TypeError:
            return 2 ** 32
    
    def __iter__(self):
        for x in self.examples:
            yield x
    
    def __getattr__(self, attr):
        if attr in self.fields:
            for x in self.examples:
                yield getattr(x, attr)