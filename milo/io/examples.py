class Example(object):
    """Defines a single training or test example.

    Stores each column of the example as an attribute.
    """
    
    @classmethod
    def fromdict(cls, data):
        ex = cls()
        for name, vals in data.items():
            if vals is not None:
                setattr(ex, name, vals)
        return ex
