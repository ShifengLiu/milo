from torch.autograd import Variable
import codecs


class CoNLL03Writer(object):
    def __init__(self, vocabs):
        self.__source_file = None
        self.vocabs = vocabs

    def start(self, file_path):
        self.__source_file = open(file_path, 'w')

    def close(self):
        self.__source_file.close()

    def write(self, batch, pred, mask, seq):
        pred_name = seq[-1]
        if isinstance(pred, Variable):
            pred = pred.data.cpu().numpy()
        else:
            pred = pred.cpu().numpy()
        seq = seq[:-1]
        infos = [getattr(batch, name).data.cpu().numpy() for name in seq]
        batch_size = batch.batch_size
        lengths = mask.data.sum(dim=1).long()
        for i in range(batch_size):
            for j in range(lengths[i]):
                out = []
                for name, info in zip(seq, infos):
                    out.append(self.vocabs[name].itos[info[i][j]])
                out.append(self.vocabs[pred_name].itos[pred[i][j]])
                self.__source_file.write(' '.join(out) + '\n')
            self.__source_file.write('\n')
