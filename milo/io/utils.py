import numpy as np
import re
import random
from contextlib import contextmanager
from copy import deepcopy


DIGIT_RE = re.compile(r"\d")
MAX_CHAR_LENGTH = 45
NUM_CHAR_PAD = 2


def norm_digits(x):
    if isinstance(x, list):
        out = [DIGIT_RE.sub("0", xx) for xx in x]
    else:
        out = DIGIT_RE.sub("0", x)
    return out


def singletonMask(batch, vocab, train):
    """

    :param batch: list of list of int
    :type batch:
    :param vocab:
    :type vocab:
    :param train:
    :type train:
    :return:
    :rtype:
    """
    if not train:
        return batch
    else:
        singltonid = vocab.singletonid
        batch = np.array(batch)
        sinletons = np.array(batch > singltonid, dtype=np.int)
        ones = np.ones_like(batch)
        noise = np.random.binomial(1, 0.5, batch.size).reshape(batch.shape)
        batch = batch * (ones - sinletons * noise)
        return batch.tolist()


def _default_unk_index():
    return 0


def hasembedding(word, vectors, lower=True, norm_digit=True):
    if vectors is None:
        return False
    if isinstance(vectors, list):
        return any([word in v.vectors.keys() or (lower and word.lower() in v.vectors.keys()) or
                    (norm_digit and norm_digits(word) in v.vectors.keys()) or
                    (lower and norm_digit and norm_digits(word.lower()) in v.vectors.keys())
                    for v in vectors])
    else:
        return word in vectors.vectors.keys() or \
               (lower and word.lower() in vectors.vectors.keys()) or \
               (norm_digit and norm_digits(word) in vectors.vectors.keys()) or \
               (lower and norm_digit and norm_digits(word.lower()) in vectors.vectors.keys())
    

class RandomShuffler(object):
    """Use random functions while keeping track of the random state to make it
    reproducible and deterministic."""

    def __init__(self, random_state=None):
        self._random_state = random_state
        if self._random_state is None:
            self._random_state = random.getstate()

    @contextmanager
    def use_internal_state(self):
        """Use a specific RNG state."""
        old_state = random.getstate()
        random.setstate(self._random_state)
        yield
        self._random_state = random.getstate()
        random.setstate(old_state)

    @property
    def random_state(self):
        return deepcopy(self._random_state)

    @random_state.setter
    def random_state(self, s):
        self._random_state = s

    def __call__(self, data):
        """Shuffle and return a new list."""
        with self.use_internal_state():
            return random.sample(data, len(data))