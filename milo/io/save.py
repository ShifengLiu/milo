__author__ = 'shifeng liu'

import torch
import os
import time
import shutil


class model_saver(object):
    def __init__(self, serialization_dir, num_serialized_models_to_keep=3):
        self.serialization_dir = serialization_dir
        self.SYSOUT = False
        if not os.path.exists(self.serialization_dir):
            os.makedirs(self.serialization_dir)
        elif len(os.listdir(self.serialization_dir))>0:
            print('WARNING! model folder is not empty!')
            self.SYSOUT = True
        
        self.num_serialized_models_to_keep = num_serialized_models_to_keep
        self.serialized_paths = []
        
    def save(self, model, epoch):
        model_path = os.path.join(self.serialization_dir, 'model_%d.pk' % (epoch))
        model_state = model.state_dict()
        torch.save(model_state, model_path)

        shutil.copyfile(model_path, os.path.join(self.serialization_dir, "model_best.pk"))
        
        if self.num_serialized_models_to_keep>0:
            self.serialized_paths.append([time.time(), model_path])
            if len(self.serialized_paths) > self.num_serialized_models_to_keep:
                paths_to_remove = self.serialized_paths.pop(0)
                for fname in paths_to_remove[1:]:
                    os.remove(fname)
    
    def reload(self, model):
        have_checkpoint = (self.serialization_dir is not None and
                           any("model_best.pk" in x for x in os.listdir(self.serialization_dir)))
        if not have_checkpoint:
            print('WARNING! no best model to load! model remains unloaded!')
        else:
            model_path = os.path.join(self.serialization_dir, 'model_best.pk')
            model_state = torch.load(model_path)
            model.load_state_dict(model_state)
        return model
