import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class LayerNorm(nn.Module):
    def __init__(self, dimension, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.gamma = torch.nn.Parameter(torch.ones(dimension))
        self.beta = torch.nn.Parameter(torch.zeros(dimension))
        self.eps = eps
    
    def forward(self, tensor):
        mean = tensor.mean(-1, keepdim=True)
        std = tensor.std(-1, unbiased=False, keepdim=True)
        return self.gamma * (tensor - mean) / (std + self.eps) + self.beta


class FFN(nn.Module):
    def __init__(self, input_dim, hidden_dim, forward_hidden_dim, dropout_prob):
        super(FFN, self).__init__()
        self.hidden_dim = hidden_dim
        self.forward_hidden_dim = forward_hidden_dim
        
        self._forward_projection = nn.Linear(input_dim, self.forward_hidden_dim)
        self._output_projection = nn.Linear(self.forward_hidden_dim, self.hidden_dim)
        self._dropout = nn.Dropout(dropout_prob)
    
    def forward(self, input_tensor):
        forward_input = self._forward_projection(input_tensor)
        forward_input = F.relu(forward_input)
        out = self._output_projection(forward_input)
        droput_out = self._dropout(out)
        return droput_out
    

class DotAttention(nn.Module):
    """
    Applies an attention mechanism on the output features from the decoder.
    .. math::
            \begin{array}{ll}
            x = context*output \\
            attn = exp(x_i) / sum_j exp(x_j) \\
            output = \tanh(w * (attn * context) + b * output)
            \end{array}
    Args:
        dim(int): The number of expected features in the output
    Inputs: output, context
        - **output** (batch, output_len, dimensions): tensor containing the output features from the decoder.
        - **context** (batch, input_len, dimensions): tensor containing features of the encoded input sequence.
        - **mask** (batch, output_len): tensor
    Outputs: output, attn
        - **output** (batch, output_len, dimensions): tensor containing the attended output features from the decoder.
        - **attn** (batch, output_len, input_len): tensor containing attention weights.
    Attributes:
        linear_out (torch.nn.Linear): applies a linear transformation to the incoming data: :math:`y = Ax + b`.
        mask (torch.Tensor, optional): applies a :math:`-inf` to the indices specified in the `Tensor`.
    Examples::
         >>> attention = DotAttention()
         >>> context = Variable(torch.randn(5, 3, 256))
         >>> output = Variable(torch.randn(5, 5, 256))
         >>> output, attn = attention(output, context)
    """
    
    def __init__(self, mask_center=False):
        super(DotAttention, self).__init__()
        self.mask_center = mask_center
    
    def forward(self, output, context, output_mask, context_mask):
        batch_size, output_len, hidden_size = output.size()
        _, context_len, _ = context.size()
        # (batch, out_len, dim) * (batch, context_len, dim) -> (batch, out_len, context_len)
        attn = torch.bmm(output, context.transpose(1, 2))
        # (batch, out_len, 1) * (batch, 1, context_len) -> (batch, out_len, context_len)
        if self.mask_center and output_len == context_len and output_len > 1:
            mask_center = Variable(torch.eye(output_len).unsqueeze(0).repeat(batch_size, 1, 1))
            single_protect = torch.ones_like(mask_center)
            single_protect.data[:, 0, 0] = 0
            mask = torch.eq(torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1) - mask_center, 1), 0)
        else:
            mask = torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1), 0)
        # Variable to Tensor
        mask = mask.data
        if mask is not None:
            attn.data.masked_fill_(mask, -float(1e30))
        
        attn = F.softmax(attn, dim=2)
        # fill nan as 0
        # attn.data[attn.data != attn.data] = 0
        
        # (batch, out_len, in_len) * (batch, in_len, dim) -> (batch, out_len, dim)
        mix = torch.bmm(attn, context)
        
        return mix, attn


class GeneralAttention(nn.Module):
    """
    Applies an attention mechanism on the output features from the decoder.
    .. math::
            \begin{array}{ll}
            x = context*output \\
            attn = exp(x_i) / sum_j exp(x_j) \\
            output = \tanh(w * (attn * context) + b * output)
            \end{array}
    Args:
        dim(int): The number of expected features in the output
    Inputs: output, context
        - **output** (batch, output_len, dimensions): tensor containing the output features from the decoder.
        - **context** (batch, input_len, dimensions): tensor containing features of the encoded input sequence.
        - **mask** (batch, output_len): tensor
    Outputs: output, attn
        - **output** (batch, output_len, dimensions): tensor containing the attended output features from the decoder.
        - **attn** (batch, output_len, input_len): tensor containing attention weights.
    Attributes:
        linear_out (torch.nn.Linear): applies a linear transformation to the incoming data: :math:`y = Ax + b`.
        mask (torch.Tensor, optional): applies a :math:`-inf` to the indices specified in the `Tensor`.
    Examples::
         >>> attention = GeneralAttention(256)
         >>> context = Variable(torch.randn(5, 3, 256))
         >>> output = Variable(torch.randn(5, 5, 256))
         >>> output, attn = attention(output, context)
    """
    
    def __init__(self, dim, mask_center=False):
        super(GeneralAttention, self).__init__()
        self.dim = dim
        self.linear = nn.Linear(dim, dim)
        self.mask_center = mask_center
    
    def forward(self, output, context, output_mask, context_mask):
        batch_size, output_len, hidden_size = output.size()
        _, context_len, _ = context.size()
        # (batch, out_len, dim) * (dim, dim) * (batch, context_len, dim) -> (batch, out_len, context_len)
        attn = torch.bmm(self.linear(output), context.transpose(1, 2))
        # (batch, out_len, 1) * (batch, 1, context_len) -> (batch, out_len, context_len)
        if self.mask_center and output_len == context_len and output_len > 1:
            mask_center = Variable(torch.eye(output_len).unsqueeze(0).repeat(batch_size, 1, 1))
            single_protect = torch.ones_like(mask_center)
            single_protect.data[:, 0, 0] = 0
            mask = torch.eq(torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1) - mask_center, 1), 0)
        else:
            mask = torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1), 0)
        # Variable to Tensor
        mask = mask.data
        if mask is not None:
            attn.data.masked_fill_(mask, -float(1e30))
        
        attn = F.softmax(attn, dim=2)
        # fill nan as 0
        # attn.data[attn.data != attn.data] = 0
        
        # (batch, out_len, in_len) * (batch, in_len, dim) -> (batch, out_len, dim)
        mix = torch.bmm(attn, context)
        
        return mix, attn


class ConcatAttention(nn.Module):
    """
    Applies an attention mechanism on the output features from the decoder.
    .. math::
            \begin{array}{ll}
            x = context*output \\
            attn = exp(x_i) / sum_j exp(x_j) \\
            output = \tanh(w * (attn * context) + b * output)
            \end{array}
    Args:
        dim(int): The number of expected features in the output
    Inputs: output, context
        - **output** (batch, output_len, dimensions): tensor containing the output features from the decoder.
        - **context** (batch, input_len, dimensions): tensor containing features of the encoded input sequence.
        - **mask** (batch, output_len): tensor
    Outputs: output, attn
        - **output** (batch, output_len, dimensions): tensor containing the attended output features from the decoder.
        - **attn** (batch, output_len, input_len): tensor containing attention weights.
    Attributes:
        linear_out (torch.nn.Linear): applies a linear transformation to the incoming data: :math:`y = Ax + b`.
        mask (torch.Tensor, optional): applies a :math:`-inf` to the indices specified in the `Tensor`.
    Examples::
         >>> attention = ConcatAttention(10)
         >>> context = Variable(torch.randn(5, 3, 256))
         >>> output = Variable(torch.randn(5, 5, 256))
         >>> output, attn = attention(output, context)
    """
    
    def __init__(self, hidden_dim, attention_dim, mask_center=False):
        super(ConcatAttention, self).__init__()
        self.mask_center = mask_center
        self.hidden_dim = hidden_dim
        self.attention_dim = attention_dim
        self.linear = nn.Linear(2 * hidden_dim, attention_dim)
        self.v = nn.Linear(self.attention_dim, 1)
    
    def forward(self, output, context, output_mask, context_mask):
        batch_size, output_len, hidden_size = output.size()
        _, context_len, _ = context.size()
        # (batch, out_len, dim) * (dim, dim) * (batch, context_len, dim) -> (batch, out_len, context_len)
        concat_output_context = torch.cat(
            [output.unsqueeze(2).repeat(1, 1, context_len, 1), context.unsqueeze(1).repeat(1, output_len, 1, 1)],
            dim=-1)
        attn = self.v(F.tanh(self.linear(concat_output_context))).squeeze(-1)
        # (batch, out_len, 1) * (batch, 1, context_len) -> (batch, out_len, context_len)
        if self.mask_center and output_len == context_len and output_len > 1:
            mask_center = Variable(torch.eye(output_len).unsqueeze(0).repeat(batch_size, 1, 1))
            single_protect = torch.ones_like(mask_center)
            single_protect.data[:, 0, 0] = 0
            mask = torch.eq(torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1) - mask_center, 1), 0)
        else:
            mask = torch.eq(output_mask.unsqueeze(2) * context_mask.unsqueeze(1), 0)
        # Variable to Tensor
        mask = mask.data
        if mask is not None:
            attn.data.masked_fill_(mask, -float(1e30))
        
        attn = F.softmax(attn, dim=2)
        # fill nan as 0
        #attn.data[attn.data != attn.data] = 0
        
        # (batch, out_len, in_len) * (batch, in_len, dim) -> (batch, out_len, dim)
        mix = torch.bmm(attn, context)
        
        return mix, attn