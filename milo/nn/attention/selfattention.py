import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


class StructuredAttention(nn.Module):
    """
    Applies an attention mechanism on the output features from the decoder.
    https://arxiv.org/pdf/1703.03130.pdf
    .. math::
            \begin{array}{ll}
            score = W_s2\tanh(W_s1 * H) \\
            attn = exp(score_i) / sum_j exp(score_j) \\
            output = attn*H
            \end{array}
    Args:
        dim(int): The number of expected features in the output
    Inputs: output, context
        - **output** (batch, output_len, dimensions): tensor containing the output features from the decoder.
        - **context** (batch, input_len, dimensions): tensor containing features of the encoded input sequence.
        - **mask** (batch, output_len): tensor
    Outputs: output, attn
        - **output** (batch, output_len, dimensions): tensor containing the attended output features from the decoder.
        - **attn** (batch, output_len, input_len): tensor containing attention weights.
    Attributes:
        linear_out (torch.nn.Linear): applies a linear transformation to the incoming data: :math:`y = Ax + b`.
        mask (torch.Tensor, optional): applies a :math:`-inf` to the indices specified in the `Tensor`.
    Examples::
         >>> attention = StructuredAttention(256, 10, 4)
         >>> output = Variable(torch.randn(5, 5, 256))
         >>> output, attn = attention(output, output_mask)
    """
    
    def __init__(self, hidden_dim, attention_dim, attention_output_dim):
        super(StructuredAttention, self).__init__()
        self.hidden_dim = hidden_dim
        self.attention_dim = attention_dim
        self.W1 = nn.Linear(hidden_dim, attention_dim)
        self.W2 = nn.Linear(attention_dim, attention_output_dim)
    
    def forward(self, output, output_mask):
        batch_size, output_len, hidden_size = output.size()
        # (batch, out_len, dim) * (dim, attention_dim) -> (batch, out_len, attention_dim)
        # (batch, out_len, attention_dim) * (attention_dim, attention_ouput_dim)->(batch, out_len, attention_ouput_dim)
        # (batch, out_len, attention_ouput_dim) -> (batch, attention_ouput_dim, out_len)
        attn = torch.exp(self.W2(F.tanh(self.W1(output))).transpose(1, 2)) * output_mask.unsqueeze(-1).transpose(1, 2)
        attn = attn / torch.norm(attn, 1, -1, True)
        # (batch, attention_ouput_dim, out_len) * (batch, out_len, dim) -> (batch, attention_ouput_dim, dim)
        mix = torch.bmm(attn, output)
        
        return mix, attn