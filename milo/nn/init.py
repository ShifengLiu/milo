__author__ = 'max'

import torch
from torch.autograd import Variable


def assign_tensor(tensor, val):
    """
    copy val to tensor
    Args:
        tensor: an n-dimensional torch.Tensor or autograd.Variable
        val: an n-dimensional torch.Tensor to fill the tensor with

    Returns:

    """
    if torch.__version__ >= '0.4':
        if isinstance(tensor, torch.nn.parameter.Parameter):
            return tensor.data.copy_(val)
    else:
        if isinstance(tensor, Variable):
            assign_tensor(tensor.data, val)
            return tensor
    return tensor.copy_(val)
