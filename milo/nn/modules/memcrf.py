__author__ = 'sliu'

import math
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from milo.nlinalg import logsumexp, logdet
from .crf import ChainCRF


class MemChainCRF(ChainCRF):
    def __init__(self, input_size, num_labels, bigram=True, mem_size=0, step=10,
                 **kwargs):
        '''

        Args:
            input_size: int
                the dimension of the input.
            num_labels: int
                the number of labels of the crf layer
            bigram: bool
                if apply bi-gram parameter.
            **kwargs:
        '''
        super(MemChainCRF, self).__init__(input_size, num_labels, bigram)
        self.mem_size = mem_size
        self.step = step
        self.init_mem()
    
    def init_mem(self):
        moduleinduda = next(self.parameters()).is_cuda
        cuda_device = next(self.parameters()).get_device() if moduleinduda else -1
        self.memory = Variable(torch.zeros(self.step, self.num_labels, self.mem_size, self.input_size),
                               volatile=False)
        self.mem_index = Variable(torch.zeros(self.step, self.num_labels).int(), volatile=False)
        if moduleinduda:
            self.memory = self.memory.cuda(cuda_device)
            self.mem_index = self.mem_index.cuda(cuda_device)
    
    def add_mem(self, new_size):
        moduleinduda = next(self.parameters()).is_cuda
        cuda_device = next(self.parameters()).get_device() if moduleinduda else -1
        
        vocab_size, _, _, _ = self.memory.size()
        increase_step = ((new_size - vocab_size) // self.step + 1) * self.step
        extend_mem = Variable(torch.zeros([increase_step, self.num_labels, self.mem_size, self.input_size]),
                              volatile=False)
        extend_index = Variable(torch.IntTensor(increase_step, self.num_labels).zero_(), volatile=False)
        if moduleinduda:
            extend_mem = extend_mem.cuda(cuda_device)
            extend_index = extend_index.cuda(cuda_device)
        self.memory = torch.cat([self.memory, extend_mem], dim=0)
        self.mem_index = torch.cat([self.mem_index, extend_index], dim=0)
        
    def forward(self, input, mask=None, mem_id=None, theta=0.0, alpha=0.0, mode='none'):
        '''

        Args:
            input: Tensor
                the input tensor with shape = [batch, length, input_size]
            mask: Tensor or None
                the mask tensor with shape = [batch, length]

        Returns: Tensor
            the energy tensor with shape = [batch, length, num_label, num_label]

        '''
        batch, length, _ = input.size()
        theta = float(theta)
        alpha = float(alpha)
        mode = str(mode)
        # increase memory for new words
        if mem_id is not None and mode != 'none':
            vocab_size, _, _, _ = self.memory.size()
            max_mem_id = torch.max(mem_id).data[0]+1
            if max_mem_id > vocab_size:
                self.add_mem(max_mem_id)
            # print('self.memory',type(self.memory), self.memory.size())
        # compute out_s by tensor dot [batch, length, input_size] * [input_size, num_label]
        # thus out_s should be [batch, length, num_label] --> [batch, length, num_label, 1]
        out_s = self.state_nn(input).unsqueeze(2)  # [batch, length, 1, num_label]

        inputincuda = input.is_cuda
        input_cude_device = input.get_device() if inputincuda else -1
        if mem_id is not None and mode != 'none':
            mem_s = Variable(input.data.new(batch, length, 1, self.num_labels).fill_(0), volatile=False)
            # print('mem_s', type(mem_s), mem_s.size())
            if inputincuda:
                mem_s = mem_s.cuda(input_cude_device)
            for b in range(batch):
                for l in range(length):
                    id_in_mem = mem_id[b][l].data[0]
                    mem_mask = np.zeros([self.num_labels, self.mem_size], dtype=np.float32)
                    for lid in range(self.num_labels):
                        mem_mask[lid, :min(self.mem_index[id_in_mem][lid].data[0], self.mem_size)] = 1.0
                    mem_mask = Variable(torch.from_numpy(mem_mask), volatile=False)
                    if inputincuda:
                        mem_mask = mem_mask.cuda(input_cude_device)
                    # print('mem_mask', type(mem_mask), mem_mask.size())
                    mem = self.memory[id_in_mem]  # [num_label, mem_size, input_size]
                    if inputincuda:
                        mem = mem.cuda(input_cude_device)
                    # print('mem1',type(mem), mem.size())
                    # print(mem)
                    mem = theta * torch.sum(torch.mul(mem, input[b][l]), 2)  # [num_label, mem_size]
                    if mode in ['global']:
                        mem += alpha
                    # print('mem2', type(mem))
                    # print(mem)
                    mem = torch.exp(mem)
                    mem = mem * mem_mask
                    mem = torch.sum(mem, 1)
                    mem_s[b][l].data = mem.data
            
            if mode in ['linear']:
                out_s = (1 - alpha) * out_s + alpha * mem
            else:
                out_s = out_s + mem_s
        
        if self.bigram:
            # compute out_s by tensor dot: [batch, length, input_size] * [input_size, num_label * num_label]
            # the output should be [batch, length, num_label,  num_label]
            out_t = self.trans_nn(input).view(batch, length, self.num_labels, self.num_labels)
            output = out_t + out_s
        else:
            # [batch, length, num_label, num_label]
            output = self.trans_matrix + out_s
        
        if mask is not None:
            output = output * mask.unsqueeze(2).unsqueeze(3)
        
        return output
    
    def loss(self, input, target, mask=None):
        '''

        Args:
            input: Tensor
                the input tensor with shape = [batch, length, input_size]
            target: Tensor
                the tensor of target labels with shape [batch, length]
            mask:Tensor or None
                the mask tensor with shape = [batch, length]

        Returns: Tensor
                A 1D tensor for minus log likelihood loss
        '''
        batch, length, _ = input.size()
        energy = self.forward(input, mask=mask)
        # shape = [length, batch, num_label, num_label]
        energy_transpose = energy.transpose(0, 1)
        # shape = [length, batch]
        target_transpose = target.transpose(0, 1)
        # shape = [length, batch, 1]
        mask_transpose = None
        if mask is not None:
            mask_transpose = mask.unsqueeze(2).transpose(0, 1)
        
        # shape = [batch, num_label]
        partition = None
        
        if input.is_cuda:
            # shape = [batch]
            batch_index = torch.arange(0, batch).long().cuda()
            prev_label = torch.cuda.LongTensor(batch).fill_(self.num_labels - 1)
            tgt_energy = Variable(torch.zeros(batch)).cuda()
        else:
            # shape = [batch]
            batch_index = torch.arange(0, batch).long()
            prev_label = torch.LongTensor(batch).fill_(self.num_labels - 1)
            tgt_energy = Variable(torch.zeros(batch))
        
        for t in range(length):
            # shape = [batch, num_label, num_label]
            curr_energy = energy_transpose[t]
            if t == 0:
                partition = curr_energy[:, -1, :]
            else:
                # shape = [batch, num_label]
                partition_new = logsumexp(curr_energy + partition.unsqueeze(2), dim=1)
                if mask_transpose is None:
                    partition = partition_new
                else:
                    mask_t = mask_transpose[t]
                    partition = partition + (partition_new - partition) * mask_t
            tgt_energy += curr_energy[batch_index, prev_label, target_transpose[t].data]
            prev_label = target_transpose[t].data
        return logsumexp(partition, dim=1) - tgt_energy
    
    def decode(self, input, mask=None, leading_symbolic=0, mem_id=None, theta=0.0, alpha=0.0, mode='none'):
        """

        Args:
            input: Tensor
                the input tensor with shape = [batch, length, input_size]
            mask: Tensor or None
                the mask tensor with shape = [batch, length]
            leading_symbolic: nt
                number of symbolic labels leading in type alphabets (set it to 0 if you are not sure)

        Returns: Tensor
            decoding results in shape [batch, length]

        """
        theta = float(theta)
        alpha = float(alpha)
        mode = str(mode)
        assert mode in ['global', 'linear', 'none']
        
        energy = self.forward(input, mask=mask, mem_id=mem_id, theta=theta, alpha=alpha, mode=mode).data
        
        # Input should be provided as (n_batch, n_time_steps, num_labels, num_labels)
        # For convenience, we need to dimshuffle to (n_time_steps, n_batch, num_labels, num_labels)
        energy_transpose = energy.transpose(0, 1)
        
        # the last row and column is the tag for pad symbol. reduce these two dimensions by 1 to remove that.
        # also remove the first #symbolic rows and columns.
        # now the shape of energies_shuffled is [n_time_steps, b_batch, t, t] where t = num_labels - #symbolic - 1.
        energy_transpose = energy_transpose[:, :, leading_symbolic:-1, leading_symbolic:-1]
        
        length, batch_size, num_label, _ = energy_transpose.size()
        
        if input.is_cuda:
            batch_index = torch.arange(0, batch_size).long().cuda()
            pi = torch.zeros([length, batch_size, num_label]).cuda()
            pointer = torch.cuda.LongTensor(length, batch_size, num_label).zero_()
            back_pointer = torch.cuda.LongTensor(length, batch_size).zero_()
        else:
            batch_index = torch.arange(0, batch_size).long()
            pi = torch.zeros([length, batch_size, num_label])
            pointer = torch.LongTensor(length, batch_size, num_label).zero_()
            back_pointer = torch.LongTensor(length, batch_size).zero_()
        
        pi[0] = energy[:, 0, -1, leading_symbolic:-1]
        pointer[0] = -1
        for t in range(1, length):
            pi_prev = pi[t - 1].unsqueeze(-1)
            pi[t], pointer[t] = torch.max(energy_transpose[t] + pi_prev, dim=1)
        
        _, back_pointer[-1] = torch.max(pi[-1], dim=1)
        for t in reversed(range(length - 1)):
            pointer_last = pointer[t + 1]
            back_pointer[t] = pointer_last[batch_index, back_pointer[t + 1]]
        
        prediction = back_pointer.transpose(0, 1) + leading_symbolic
        
        if mem_id is not None and mode != 'none':
            for b in range(batch_size):
                for l in range(length):
                    id_in_mem = mem_id[b][l].data[0]
                    pre = prediction[b][l]
                    ix = self.mem_index[id_in_mem][pre].data[0] % self.mem_size
                    self.memory[id_in_mem][pre][ix].data = input[b][l].data
                    self.mem_index[id_in_mem][pre].data += 1
        
        return prediction


