import torch
import torch.nn as nn
import torch.nn.functional as F


# multiclass SVM
class multiClassHingeLoss(nn.Module):
    def __init__(self, p=1, margin=1, weight=None, size_average=True):
        super(multiClassHingeLoss, self).__init__()
        self.p = p
        self.margin = margin
        # weight for each class, size=n_class, variable containing FloatTensor,cuda,reqiures_grad=False
        self.weight = weight
        self.size_average = size_average

    def forward(self, output, y):
        # output: (batch_size, num_labels)
        output_y = output[torch.arange(0, y.size()[0]).long(), y.data].view(-1, 1)  # view for transpose
        # margin - output[y] + output[i]
        loss = output - output_y + self.margin  # contains i=y
        # remove i=y items
        loss[torch.arange(0, y.size()[0]).long(), y.data] = 0
        # max(0,_)
        loss = F.relu(loss)
        # ^p
        if self.p != 1:
            loss = torch.pow(loss, self.p)
        # add weight
        if self.weight is not None:
            loss = loss * self.weight
        # sum up
        loss = torch.sum(loss)
        if self.size_average:
            loss /= output.size()[0]
        return loss
    

# base SVM
class SVM(nn.Module):
    def __init__(self, input_size, num_labels, p=1, margin=1, weight=None, size_average=True):
        super(SVM, self).__init__()
        self.input_size = input_size
        self.num_labels = num_labels
        
        self.fc = nn.Linear(input_size, num_labels)
        self.hingeloss = multiClassHingeLoss(p, margin, weight, size_average)
        self.reset_parameters()
        
    def reset_parameters(self):
        nn.init.constant(self.fc.bias, 0.1)
        nn.init.xavier_uniform(self.fc.weight)
        
    def forward(self, x):
        output = self.fc(x)
        return output
    
    def loss(self, input_, target_):
        return self.hingeloss(input_, target_)