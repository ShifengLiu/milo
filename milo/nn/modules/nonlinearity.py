import torch
import numpy as np


'''
https://github.com/hendrycks/GELUs
'''
if torch.__version__ >= '0.4.1':
    def gelu(x):
        return torch.mul(x, torch.erfc(-x / float(np.sqrt(2.))) / 2.)

'''
https://openreview.net/forum?id=Bk0MRI5lg
'''
def gelu_fast(_x):
    return 0.5 * _x * (1 + torch.tanh(float(np.sqrt(2 / np.pi)) * (_x + 0.044715 * torch.pow(_x, 3))))


def silu(_x):
    return _x * torch.sigmoid(_x)