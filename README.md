# milo

Machine Learning IO based on torchtext

```json
{
  "reader":{
    "path": {
      "train": "/Users/sliu/PycharmProjects/neuronlp2/neuronlp2/data/train_std.txt",
      "dev": "/Users/sliu/PycharmProjects/neuronlp2/neuronlp2/data/dev_std.txt",
      "test": "/Users/sliu/PycharmProjects/neuronlp2/neuronlp2/data/test_std.txt"},
    "column_size": 5,
    "batch_size": 16,
    "annotate_batch_size": 128,
    "infos": {
      "word": {
        "column": 0,
        "usechar": 1,
        "embedding_path": "/Users/sliu//word_vectors/glove.6B.100d.txt",
        "lower": 0,
        "normalize_digits": 1,
        "singleton":1,
        "singleton_replace":0.0,
        "maintrain_string": 1},
      "wordid": {"column": 0}
    },
    "labels": {
      "ner":{"column": -1}},
    "device": 0
  }
}
```